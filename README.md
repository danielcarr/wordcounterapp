#### Question 1

Please develop a simple Android application that is used to determine the frequency of each word
that appears in the book *War and Peace* by Leo Tolstoy. Your application must be able to retrieve
the file on [http://www.gutenberg.org/files/2600/2600.txt](http://www.gutenberg.org/files/2600/2600.txt) from the local device storage ideally
using a user interface that allows the user to select the input file from local storage. The user
then taps a button which starts processing the file on your device. Your application will then
display the desired output. A user must be able to process the same file or any other files as
many times as they want.

#### Output:

```
Most frequent word: {word} occurred {x} times
Most frequent 7 character word: {word} occurred {x} times
```

#### Question 2

The responsiveness of a mobile application is one of the priorities in determining acceptable
usability. Please write a few paragraphs describing the performance achieved and possible ways to
improve your application's responsiveness.
