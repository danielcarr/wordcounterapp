package za.co.danielcarr.countwordsapp.workers;

import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import java.io.File;

public abstract class FileSelectionListener implements AdapterView.OnItemClickListener {

    public abstract void onFileSelected(File file);

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        TextView selectedCell = (TextView) view;
        String filePath = selectedCell.getText().toString();
        onFileSelected(new File(filePath));
    }
}
