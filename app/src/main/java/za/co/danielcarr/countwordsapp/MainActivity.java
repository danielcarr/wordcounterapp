package za.co.danielcarr.countwordsapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

import za.co.danielcarr.countwordsapp.services.ProcessFileService;
import za.co.danielcarr.countwordsapp.workers.FileSelectionListener;

public class MainActivity extends AppCompatActivity {
    private ListView mFileSelector;
    private ArrayAdapter<File> mFileListAdapter;
    private FileSelectionListener mFileSelectionListener;

    private static final int sNUMBER_OF_CHARACTERS_FOR_SECONDARY_RESULT = 7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IntentFilter intentFilter = new IntentFilter(ProcessFileService.ACTION_RETURN_BROADCAST);
        LocalBroadcastManager.getInstance(this).registerReceiver(new WordCountReceiver(), intentFilter);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setWordFrequencyLabelVisibility(false);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.button_new_file);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectFileToProcess();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_reset) {
            TextView uploadFileLabel = (TextView) findViewById(R.id.label_uploaded_file);
            uploadFileLabel.setText(R.string.introductory_call);
            setProgressIndication(false);
            resetFileList();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void selectFileToProcess() {
        setWordFrequencyLabelVisibility(false);
        ListView fileSelector = getFileSelector();
        fileSelector.setVisibility(View.VISIBLE);
    }

    private File[] getAllPublicTextFilesInDirectory(File directory) {
        ArrayList<File> fileList = new ArrayList<File>();
        File[] filesInDirectory = directory.listFiles();
        for (File file : filesInDirectory) {
            if (file.isDirectory()) {
                File[] filesInInnerDirectory = getAllPublicTextFilesInDirectory(file);
                for (File innerFile : filesInInnerDirectory) {
                    fileList.add(innerFile);
                }
            } else if (file.canRead() && file.getName().endsWith(".txt")) {
                fileList.add(file);
            }
        }
        return fileList.toArray(new File[fileList.size()]);
    }

    private void setCurrentFileHeadline(String filePath) {
        String fileIdentifierHeadline = getString(R.string.file_read_error);
        if (filePath != null) {
            int beginningOfFileName = filePath.lastIndexOf(File.separator) + 1;
            String fileName = filePath.substring(beginningOfFileName);
            fileIdentifierHeadline = getString(R.string.current_file_identifier, fileName);
        }
        TextView currentFileIndicator = (TextView) findViewById(R.id.label_uploaded_file);
        currentFileIndicator.setText(fileIdentifierHeadline);
    }

    private void setWordFrequencyLabels(int frequency, String mostFrequentWord,
                                        int specialFrequency, String mostFrequentSpecialWord) {
        TextView totalWordCountLabel = (TextView) findViewById(R.id.label_total_word_count);
        String totalWordCountLabelText = getString(R.string.result_primary, mostFrequentWord, frequency);
        totalWordCountLabel.setText(totalWordCountLabelText);
        if (specialFrequency != 0 && mostFrequentSpecialWord != null) {
            TextView specialWordCountLabel = (TextView) findViewById(R.id.label_special_word_count);
            String specialWordCountLabelText = getString(R.string.result_secondary,
                    sNUMBER_OF_CHARACTERS_FOR_SECONDARY_RESULT, mostFrequentSpecialWord, specialFrequency);
            specialWordCountLabel.setText(specialWordCountLabelText);
        }
    }

    private void setWordFrequencyLabelVisibility(boolean visible) {
        int visibility = visible ? View.VISIBLE : View.INVISIBLE;
        TextView textView = (TextView) findViewById(R.id.label_total_word_count);
        textView.setVisibility(visibility);
        textView = (TextView) findViewById(R.id.label_special_word_count);
        textView.setVisibility(visibility);
    }

    private void resetFileList() {
        ListView fileSelector = getFileSelector();
        fileSelector.setVisibility(View.GONE);
        setWordFrequencyLabelVisibility(false);
        TextView uploadFileLabel = (TextView) findViewById(R.id.label_uploaded_file);
        uploadFileLabel.setText(getString(R.string.introductory_call));
    }

    private ListView getFileSelector() {
        if (mFileSelector == null) {
            mFileSelector = (ListView) findViewById(R.id.file_selector);
            mFileSelector.setOnItemClickListener(getFileSelectionListener());
        }
        mFileSelector.setAdapter(getFileAdapter());
        return mFileSelector;
    }

    private ArrayAdapter<File> getFileAdapter() {
        int fileSelectionCellId = R.layout.file_selection_cell;
        File topLevelPublicStorage = Environment.getExternalStorageDirectory();
        File[] fileArray = getAllPublicTextFilesInDirectory(topLevelPublicStorage);
        mFileListAdapter = new ArrayAdapter<File>(this, fileSelectionCellId, fileArray);
        return mFileListAdapter;
    }

    private void setProgressIndication(boolean inProgress) {
        ProgressBar progressIndicator = (ProgressBar) findViewById(R.id.progressBar);
        progressIndicator.setVisibility(inProgress ? View.VISIBLE : View.GONE);
    }

    private FileSelectionListener getFileSelectionListener() {
        if (mFileSelectionListener == null) {
            mFileSelectionListener = new FileSelectionListener() {
                @Override
                public void onFileSelected(File file) {
                    String filePath = file.getPath();
                    setCurrentFileHeadline(filePath);
                    ListView fileListView = (ListView) findViewById(R.id.file_selector);
                    fileListView.setVisibility(View.GONE);
                    setProgressIndication(true);
                    ProcessFileService.startActionProcessFile(
                            fileListView.getContext(),
                            filePath,
                            sNUMBER_OF_CHARACTERS_FOR_SECONDARY_RESULT
                    );
                }
            };
        }
        return mFileSelectionListener;
    }

    private class WordCountReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String mostCommonWord = intent.getStringExtra(
                    ProcessFileService.EXTRA_RETURN_VALUE_MOST_FREQUENT_WORD);
            int frequencyOfMostCommonWord = intent.getIntExtra(
                    ProcessFileService.EXTRA_RETURN_VALUE_FREQUENCY_OF_MOST_FREQUENT_WORD, 0);
            String mostCommonSpecialWord = intent.getStringExtra(
                    ProcessFileService.EXTRA_RETURN_VALUE_MOST_FREQUENT_SPECIAL_WORD);
            int frequencyOfMostCommonSpecialWord = intent.getIntExtra(
                    ProcessFileService.EXTRA_RETURN_VALUE_FREQUENCY_OF_MOST_FREQUENT_SPECIAL_WORD, 0);
            setWordFrequencyLabels(frequencyOfMostCommonWord, mostCommonWord,
                    frequencyOfMostCommonSpecialWord, mostCommonSpecialWord);
            setWordFrequencyLabelVisibility(true);
            setProgressIndication(false);
        }
    }
}
