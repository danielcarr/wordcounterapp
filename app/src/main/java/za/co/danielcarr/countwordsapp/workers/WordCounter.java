package za.co.danielcarr.countwordsapp.workers;

import java.util.ArrayList;
import java.util.regex.Pattern;

import za.co.danielcarr.countwordsapp.models.WordLengthList;

public class WordCounter {
    public final static String INTER_WORD_REGEX = "[\\W]";
    public final static Pattern INTER_WORD_PATTERN = Pattern.compile(INTER_WORD_REGEX);

    private final WordLengthList mWordLengthList;
    private TextProcessingHandler mCompletionDelegate;

    public WordCounter() {
        this(null, null);
    }

    public WordCounter(WordLengthList existingWordLengthList) {
        if (existingWordLengthList == null) {
            existingWordLengthList = new WordLengthList();
        }
        this.mWordLengthList = existingWordLengthList;
    }

    public WordCounter(TextProcessingHandler completionDelegate) {
        WordLengthList existingWordLengthList = null;
        if (completionDelegate != null) {
            this.mCompletionDelegate = completionDelegate;
            existingWordLengthList = completionDelegate.getStructure();
            if (existingWordLengthList == null) {
                existingWordLengthList = new WordLengthList();
            }
        }
        this.mWordLengthList = existingWordLengthList;
    }

    public WordCounter(WordLengthList existingWordLengthList, TextProcessingHandler completionDelegate) {
        this(existingWordLengthList);
        this.mCompletionDelegate = completionDelegate;
    }

    public void setCompletionDelegate(TextProcessingHandler delegate) {
        mCompletionDelegate = delegate;
    }

    public int processText(String text) {
        String[] wordList = getWordListFromText(text);
        int numberOfWordsProcessed = wordList.length;
        updateFrequencyListWithWords(wordList);
        return numberOfWordsProcessed;
    }

    private String[] getWordListFromText(String text) {
        String[] regexSplitWordList = INTER_WORD_PATTERN.split(text.toLowerCase());
        ArrayList<String> mutableWordList = new ArrayList<>();
        for (String word : regexSplitWordList) {
            if (!word.equals("")) {
                mutableWordList.add(word);
            }
        }
        return mutableWordList.toArray(new String[mutableWordList.size()]);
    }

    private void updateFrequencyListWithWords(String[] wordList) {
        for (String word : wordList) {
            mWordLengthList.addWord(word);
        }
        mCompletionDelegate.TextProcessedWithStructure(mWordLengthList);
    }
}
