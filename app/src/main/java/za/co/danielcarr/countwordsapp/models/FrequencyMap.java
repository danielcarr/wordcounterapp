package za.co.danielcarr.countwordsapp.models;

import java.util.ArrayList;
import java.util.HashMap;

public class FrequencyMap {
    private final HashMap<String, Integer> mWordMap;
    private final ArrayList<String> mFrequencyList;
    private String mMostCommonWord;
    private int mFrequencyOfMostFrequentWord;

    public FrequencyMap() {
        this.mWordMap = new HashMap<String, Integer>();
        this.mFrequencyList = new ArrayList<String>();
        this.mMostCommonWord = "";
        this.mFrequencyOfMostFrequentWord = 0;
    }

    public void addNewWord(String word) {
        mWordMap.put(word, 1);
        mFrequencyList.add(word);
    }

    public int incrementFrequencyOfWord(String word) {
        Integer frequency = mWordMap.get(word);
        if (frequency == null) {
            addNewWord(word);
            frequency = 1;
        } else {
            frequency += 1;
            mWordMap.put(word, frequency);
            repositionWord(word);
        }
        if (frequency > mFrequencyOfMostFrequentWord) {
            setMostFrequentWord(word, frequency);
        }
        return frequency;
    }

    public String getMostCommonWord() {
        //return mFrequencyList.get(0);
        return mMostCommonWord;
    }

    public Integer getFrequencyOfMostCommonWord() {
        //return frequencyOfWord(mostCommonWord());
        return mFrequencyOfMostFrequentWord;
    }

    public Integer getFrequencyOfWord(String word) {
        Integer frequency = 0;
        if (mFrequencyList.contains(word)) {
            frequency = mWordMap.get(word);
        }
        return frequency;
    }

    private void setMostFrequentWord(String word, int frequency) {
        mMostCommonWord = word;
        mFrequencyOfMostFrequentWord = frequency;
    }

    private void repositionWord(String word) {
        Integer frequency = mWordMap.get(word);
        Integer currentIndex = mFrequencyList.indexOf(word);
        Integer newIndex = currentIndex;
        if (currentIndex > 0) {
            String previousWord;
            Integer otherFrequency;
            do {
                newIndex -= 1;
                if (newIndex == 0) {
                    break;
                }
                previousWord = mFrequencyList.get(newIndex);
                otherFrequency = mWordMap.get(previousWord);
            } while (otherFrequency > frequency ||
                    otherFrequency == frequency && word.compareToIgnoreCase(previousWord) > 0);
        }
        if (currentIndex != newIndex) {
            mFrequencyList.remove(currentIndex.intValue());
            mFrequencyList.add(newIndex, word);
        }
    }
}
