package za.co.danielcarr.countwordsapp.workers;

import za.co.danielcarr.countwordsapp.models.WordLengthList;

public interface TextProcessingHandler {
    WordLengthList getStructure();
    void TextProcessedWithStructure(WordLengthList structure);
}
