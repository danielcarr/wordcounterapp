package za.co.danielcarr.countwordsapp.models;

import java.util.ArrayList;

public class WordLengthList {
    private final ArrayList<FrequencyMap> mArrayList;
    private String mMostCommonWord;
    private int mFrequencyOfMostCommonWord;

    public WordLengthList() {
        this.mArrayList = new ArrayList<FrequencyMap>();
        this.mMostCommonWord = "";
        this.mFrequencyOfMostCommonWord = 0;
    }

    public void addWord(String word) {
        int index = word.length() + 1;
        FrequencyMap map = index >= mArrayList.size() ? null : mArrayList.get(index);
        if (map == null) {
            map = new FrequencyMap();
        }
        int frequency = map.incrementFrequencyOfWord(word);
        fillArrayListUpTo(index);
        mArrayList.set(index, map);

        int frequencyOfMostFrequentWord = getFrequencyOfMostFrequentWord();
        if (frequency > frequencyOfMostFrequentWord ||
                (frequency == frequencyOfMostFrequentWord &&
                        word.compareToIgnoreCase(getMostFrequentWord()) > 0)) {
            setMostCommonWord(word, frequency);
        }
    }

    public String getMostFrequentWord() {
        return mMostCommonWord;
    }

    public int getFrequencyOfMostFrequentWord() {
        return mFrequencyOfMostCommonWord;
    }

    public String getMostFrequentWordOfLength(int length) {
        FrequencyMap map = getWordsOfLength(length);
        String mostCommonWord = null;
        if (map != null) {
            mostCommonWord = map.getMostCommonWord();
        }
        return mostCommonWord;
    }

    public int getFrequencyOfMostFrequentWordOfLength(int length) {
        FrequencyMap map = getWordsOfLength(length);
        int returnValue = 0;
        if (map != null) {
            returnValue = map.getFrequencyOfMostCommonWord();
        }
        return returnValue;
    }

    private FrequencyMap getWordsOfLength(int length) {
        return mArrayList.get(length + 1);
    }

    private void setMostCommonWord(String word, int frequency) {
        mMostCommonWord = word;
        mFrequencyOfMostCommonWord = frequency;
    }

    private void fillArrayListUpTo(int index) {
        for (int i = mArrayList.size() - 1; i < index; ++i) {
            mArrayList.add(null);
        }
    }
}
