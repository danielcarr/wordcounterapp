package za.co.danielcarr.countwordsapp.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Pattern;

import za.co.danielcarr.countwordsapp.models.WordLengthList;
import za.co.danielcarr.countwordsapp.workers.TextProcessingHandler;
import za.co.danielcarr.countwordsapp.workers.WordCounter;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * helper methods.
 */
public class ProcessFileService extends IntentService implements TextProcessingHandler {
    private static final String ACTION_PROCESS_FILE = "za.co.danielcarr.countwordsapp.services.action.PROCESS_FILE";

    private static final String EXTRA_FILE_PATH = "za.co.danielcarr.countwordsapp.services.extra.FILE_PATH";
    private static final String EXTRA_SPECIAL_WORD_LENGTH = "za.co.danielcarr.countwordsapp.services.SPECIAL_WORD_LENGTH";

    // Constants for the local broadcast intent containing the results of processing the file
    public static final String ACTION_RETURN_BROADCAST =
            "za.co.danielcarr.countwordsapp.services.intent.ACTION_RETURN_BROADCAST";
    public static final String EXTRA_RETURN_VALUE_MOST_FREQUENT_WORD =
            "za.co.danielcarr.countwordsapp.services.extra.MOST_FREQUENT_WORD";
    public static final String EXTRA_RETURN_VALUE_FREQUENCY_OF_MOST_FREQUENT_WORD =
            "za.co.danielcarr.countwordsapp.services.extra.FREQUENCY_OF_MOST_FREQUENT_WORD";
    public static final String EXTRA_RETURN_VALUE_MOST_FREQUENT_SPECIAL_WORD =
            "za.co.danielcarr.countwordsapp.service.extra.MOST_FREQUENT_SPECIAL_WORD";
    public static final String EXTRA_RETURN_VALUE_FREQUENCY_OF_MOST_FREQUENT_SPECIAL_WORD =
            "za.co.danielcarr.countwordsapp.services.extra.FREQUENCY_OF_MOST_FREQUENT_SPECIAL_WORD";

    private int mSpecialWordLength;
    private WordCounter mWordCounter;
    private WordLengthList mWordLengthList;

    public ProcessFileService() {
        super("ProcessFileService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionProcessFile(Context context, String filePath, int specialWordLength) {
        Intent intent = new Intent(context, ProcessFileService.class);
        intent.setAction(ACTION_PROCESS_FILE);
        intent.putExtra(EXTRA_FILE_PATH, filePath);
        intent.putExtra(EXTRA_SPECIAL_WORD_LENGTH, specialWordLength);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_PROCESS_FILE.equals(action)) {
                final File fileToRead = new File (intent.getStringExtra(EXTRA_FILE_PATH));
                mSpecialWordLength = intent.getIntExtra(EXTRA_SPECIAL_WORD_LENGTH, 0);
                mWordLengthList = new WordLengthList();
                mWordCounter = new WordCounter(mWordLengthList, this);
                handleActionCountWordsInFile(fileToRead);
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionCountWordsInFile(File fileToRead) {
        final int MAX_BYTES_TO_READ = 1024;
        final Pattern INTER_WORD_PATTERN = WordCounter.INTER_WORD_PATTERN;
        int lastIndex = 0;
        char[] buffer = new char[MAX_BYTES_TO_READ];
        try {
            FileReader fileReader = new FileReader(fileToRead);
            do {
                lastIndex = fileReader.read(buffer, 0, MAX_BYTES_TO_READ);
                while (lastIndex > 0) {
                    String characterAtLastIndex = new String(buffer, lastIndex - 1, 1);
                    if (INTER_WORD_PATTERN.matcher(characterAtLastIndex).matches()) {
                        break;
                    } else {
                        lastIndex -= 1;
                    }
                }
                String text = new String(buffer, 0, buffer.length);
                mWordLengthList = addWordsToWordLengthList(text);
            } while (lastIndex > -1);
            returnResults();
        } catch (FileNotFoundException fne) {
            fne.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private WordLengthList addWordsToWordLengthList(String text) {
        mWordCounter.processText(text);
        return mWordLengthList;
    }

    private void returnResults() {
        Intent responseIntent = new Intent(ACTION_RETURN_BROADCAST);
        responseIntent.putExtra(EXTRA_RETURN_VALUE_MOST_FREQUENT_WORD,
                mWordLengthList.getMostFrequentWord());
        responseIntent.putExtra(EXTRA_RETURN_VALUE_FREQUENCY_OF_MOST_FREQUENT_WORD,
                mWordLengthList.getFrequencyOfMostFrequentWord());
        responseIntent.putExtra(EXTRA_RETURN_VALUE_MOST_FREQUENT_SPECIAL_WORD,
                mWordLengthList.getMostFrequentWordOfLength(mSpecialWordLength));
        responseIntent.putExtra(EXTRA_RETURN_VALUE_FREQUENCY_OF_MOST_FREQUENT_SPECIAL_WORD,
                mWordLengthList.getFrequencyOfMostFrequentWordOfLength(mSpecialWordLength));
        LocalBroadcastManager.getInstance(this).sendBroadcast(responseIntent);
    }

    @Override
    public WordLengthList getStructure() {
        return mWordLengthList;
    }

    @Override
    public void TextProcessedWithStructure(WordLengthList structure) {
        mWordLengthList = structure;
    }
}
